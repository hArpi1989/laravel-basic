<?php

namespace App\Http\Controllers;

use App\AddressItem;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Address;
use Illuminate\Support\Facades\Input;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $page = Input::get('page');
        $total = Input::get('total');
        $limit = Input::get('limit');
        $orderBy = Input::get('orderBy');

        $addresses = Address::limit(!empty($limit) ? $limit : 1);

        if (!empty($orderBy)) {
            $orderBy = explode('|', $orderBy);
            $addresses = $addresses->orderBy($orderBy[0], $orderBy[1]);
        }
        $addresses = $addresses->get();

        if (!empty($addresses)) {

            $returnData = array();

            foreach ($addresses as $address) {
                $returnData['data'][$address->id] = [
                    'id' => $address->id,
                    'name' => $address->name,
                    'created' => $address->created,
                    'created_by' => $address->created_by,
                    'modified' => $address->modified,
                    'modified_by' => $address->modified_by
                ];

                $address_items = AddressItem::where('aid', $address->id)->get();
                foreach ($address_items as $item) {
                    $returnData['data'][$address->id]['items'][] = [
                        'key' => $item->key,
                        'value' => $item->value
                    ];
                }
            }
            return $returnData;
        } else {
            return back()->withError('Error');
        }

    }

    public function show($id)
    {


        try {
            $address = Address::find($id);

            if (!empty($address)) {

                $returnData = array();
                $returnData['data'][$id] = [
                    'id' => $address->id,
                    'name' => $address->name,
                    'created' => $address->created,
                    'created_by' => $address->created_by,
                    'modified' => $address->modified,
                    'modified_by' => $address->modified_by
                ];

                $address_items = AddressItem::where('aid', $id)->get();
                foreach ($address_items as $item) {
                    $returnData['data'][$id]['items'][] = [
                        'key' => $item->key,
                        'value' => $item->value
                    ];
                }

                return $returnData;
            }
        } catch (ModelNotFoundException $e) {
            $title = "Error retrieving data";
            $message = "Not found!";
            $success = 0;
            return Response::json(['title' => $title, 'message' => $message, 'success' => $success], 500);
        }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
