<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    protected $fillable = ['id','name', 'note', 'created','created_by', 'modified', 'modified_by'];
}
