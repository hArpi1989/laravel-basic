<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressItem extends Model
{
    protected $table = 'address_item';
    protected $fillable = ['aid', 'key', 'value', 'params', 'require','sequence', 'note', 'created','created_by', 'modified', 'modified_by'];
}
